### About me 👋

#### Languages

<div style="display:flex">
<img src="https://raw.githubusercontent.com/dominikfladung/dominikfladung/master/ColoredBadges/svg/dev/languages/js.svg" alt="Vue" style="vertical-align:top margin:6px 4px">

<img src="https://raw.githubusercontent.com/dominikfladung/dominikfladung/master/ColoredBadges/svg/dev/languages/php.svg" alt="Vue" style="vertical-align:top margin:6px 4px">

<img src="https://raw.githubusercontent.com/dominikfladung/dominikfladung/master/ColoredBadges/svg/dev/languages/python.svg" alt="Vue" style="vertical-align:top margin:6px 4px">

<img src="https://raw.githubusercontent.com/dominikfladung/dominikfladung/master/ColoredBadges/svg/dev/languages/csharp_dotnet.svg" alt="Vue" style="vertical-align:top margin:6px 4px">

<img src="https://raw.githubusercontent.com/dominikfladung/dominikfladung/master/ColoredBadges/svg/dev/languages/swift.svg" alt="Vue" style="vertical-align:top margin:6px 4px">
</div>

#### Frameworks

<img src="https://raw.githubusercontent.com/dominikfladung/dominikfladung/master/ColoredBadges/svg/dev/frameworks/vue.svg" alt="Vue" style="vertical-align:top margin:6px 4px">

#### Visitor Count

![Visitor Count](https://profile-counter.glitch.me/dominikfladung/count.svg)

![My github stats](https://github-readme-stats.vercel.app/api?username=dominikfladung&show_icons=true&title_color=fff&icon_color=79ff97&text_color=9f9f9f&bg_color=151515)


<!--
**dominikfladung/dominikfladung** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->
